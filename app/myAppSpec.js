describe("create services", function() {
	var app = new newApp();
	app.config('confA', function(){}); 
	app.service('a', function(){}); 
	app.service('b', function(){}); 
	app.service('c', function(a,b){}); 
	app.service('d', function(a){}); 
	
	it("registered", function() {
		expect( typeof app._fns['a'] ).toBe('object');
		expect( typeof app._fns['a'].fn ).toBe('function');
		expect( app._fns['c'].deps.constructor ).toBe(Array);
		expect( app._fns['c'].deps[0] ).toBe('a');
	});
	it("duplicate", function() {
		expect( function(){ app.service('a', function(){}); } ).toThrow(new Error('service a exists'));
	});
	it("override", function() {
		expect( function(){ app.service('a', true, function(){}); } ).not.toThrow(new Error('service a exists'));
	});
	it("dependency name = fnName", function() {
		expect( function(){ app.service('e', function(e){}); } ).toThrow(new Error('dependency e is te same function'));
	});
	it("dependency not registered", function() {
		expect( function(){ 
			app.service('f', function(notExistServiceName){}); 
			app.run(); 
		} ).toThrow(new Error('service notExistServiceName not registered'));
	});
	
});
describe("run services", function() {
	it("dependency loop", function() {
		expect( function(){ 
			var app = new newApp();
			app.service('g', function(h){}); 
			app.service('h', function(i,g){});
			app.service('i', function(){});
			app.service('j', function(g){});
			app.run(); 
		} ).toThrow(new Error('dependency loop: g - h - g'));
	});
	
	it("add services after run()", function() {
		expect( function(){ 
			var app = new newApp();
			app.service('g', function(h){}); 
			app.service('h', function(){});
			app.run();
			app.service('i', function(){});
			app.service('j', function(g){});
			app.run();
		} ).not.toThrow(new Error('service notExistServiceName not registered'));
	});
});
