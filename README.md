CREATE-SERVICE-JS
----------------

JavaScript project - easy create your js app with dependencies on multi-file project.

Download
-------
myApp.js - is all you need, set this as first in your files

How To Use
--------

EXAMPLE:

* create services .service(name, override, fn)

@name: string, serviceName

@override: bool, optional, override existing service(even have a running)

@fn: function, parameters are a dependencies other services

app.service('g', function(h){console.log(h.name);return 'g'});

app.service('h', function(){return {name:'h'}});

app.config('confA', function(){return {name: 'confa'}});

* example override service

app.service('h', true, function(){return {name:'hh'}});

app.service('j', function(){return {name:'j'}});

* execute configs and services

app.run();

* create other services after run()

app.service('i', function(h){console.log(h.name);return 'i'});

* we can override running services
* our services before second run() will use overrides services

app.service('h', true, function(j){return {name:'hhh'}});

app.service('h', true, function(){return {name:'hh'}});

* execute second services

app.run();

and so on..

FEATURES
--------

* $global('name', api)

** name - optional, if not exist use service name

** api - any type of data (string, array, object, ..)

** app.service('myServ', function($global){ ...  $global('name', {...}); })

** app.myServ => api